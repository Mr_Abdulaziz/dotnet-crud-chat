using System.Reflection;
using chat_systems.Authorization.Extensions;
using chat_systems.Shared.Options;
using chat_systems.Shared.SignalR;
using chat_systems.Shared.Services;
using chat_systems.Message.Services;
using chat_systems.Shared.Interfaces;
using chat_systems.Message.Repositories;
using chat_systems.Shared.Services.Interfaces;
using chat_systems.Message.Services.Interfaces;
using chat_systems.Message.Repositories.Interfaces;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddSignalR();
builder.Services.AddMediatR(cfg => cfg.RegisterServicesFromAssembly(Assembly.GetExecutingAssembly()));

builder.Services.AddSingleton<IChatHub, ChatHub>();

builder.Services.Configure<PostgreSqlOptions>(builder.Configuration.GetSection(PostgreSqlOptions.SECTION_NAME));

builder.Services.AddAuthServices(builder.Configuration);

builder.Services.AddSingleton<IMessageService, MessageService>();
builder.Services.AddSingleton<IMessageRepository, MessageRepository>();
builder.Services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
builder.Services.AddSingleton<IUriService>(serviceProvider =>
{
    var accessor = serviceProvider.GetRequiredService<IHttpContextAccessor>();
    var request = accessor.HttpContext?.Request;
    var uri = string.Concat(request?.Scheme, "://", request?.Host.ToUriComponent());
    return new PaginationUriService(uri);
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseCors(x => x
    .AllowAnyOrigin()
    .AllowAnyHeader()
    .AllowAnyMethod());

app.UseHttpsRedirection();

app.UseRouting();

app.MapControllers();

app.Run();
