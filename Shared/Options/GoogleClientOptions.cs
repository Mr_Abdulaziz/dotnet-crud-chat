using chat_systems.Authorization.Clients.Options;

namespace chat_systems.Shared.Options;

public class GoogleClientOptions
{
    public const string SECTION_NAME = "GoogleClientOptions";
    
    public FirebaseClientOptions FirebaseApp { get; set; }
}