namespace chat_systems.Shared.Options;

public class PostgreSqlOptions
{
    public const string SECTION_NAME = "PostgreSqlOptions";
    
    public string ConnectionString { get; set; }
}