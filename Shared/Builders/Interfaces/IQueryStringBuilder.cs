namespace chat_systems.Shared.Builders.Interfaces;

public interface IQueryStringBuilder
{
    IQueryStringBuilder With(string key, string value);

    string Build();
}