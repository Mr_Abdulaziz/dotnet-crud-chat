using System.Text;
using System.Web;
using chat_systems.Shared.Builders.Interfaces;

namespace chat_systems.Shared.Builders;

public class QueryStringBuilder : IQueryStringBuilder
{
    private readonly StringBuilder _stringBuilder = new ();
    
    public IQueryStringBuilder With(string key, string value)
    {
        _stringBuilder.Append(_stringBuilder.Length is 0 ? $"?{key}" : $"&{key}");
        _stringBuilder.Append('=');
        _stringBuilder.Append(HttpUtility.UrlEncode(value));

        return this;
    }

    public string Build() => _stringBuilder.ToString();
}