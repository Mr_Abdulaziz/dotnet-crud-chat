namespace chat_systems.Shared.DTOs;

public record PaginatedResponseWrapper<T>
{
    private int currentPage;
    
    public int CurrentPage
    {
        get => currentPage;
        set => currentPage = value is 0 ? 1 : value;
    }

    public int PageSize { get; set; }
    
    public string? PreviousPage { get; set; }
    
    public string? NextPage { get; set; }

    public List<T> Data { get; init; } = new();
};