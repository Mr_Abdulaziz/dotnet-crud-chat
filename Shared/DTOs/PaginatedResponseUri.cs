namespace chat_systems.Shared.DTOs;

public record PaginatedResponseUri
{
    public Uri? NextPage { get; set; }
    
    public Uri? PreviousPage { get; set; }
}