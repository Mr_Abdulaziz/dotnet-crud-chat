using Microsoft.AspNetCore.Mvc.Filters;

namespace chat_systems.Shared.Attributes;

[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
public class AuthorizeAttribute : Attribute, IAuthorizationFilter
{
    public bool IsDefaultClaimsRequired { get; set; } = true;
    
    public string Roles { get; set; }

    public AuthorizeAttribute(params string[] roles) => Roles = string.Join(",", roles);
    
    
    public void OnAuthorization(AuthorizationFilterContext context)
    {
        if (!IsDefaultClaimsRequired) return;

        var allowedRoles = Roles.Trim().Split(",").ToList();
        var userRoleClaims = context.HttpContext.Items["Role"];
        var userRoleClaimsAsString = userRoleClaims?.ToString();

        if (userRoleClaims is null) throw new Exception("");
        
    }
}