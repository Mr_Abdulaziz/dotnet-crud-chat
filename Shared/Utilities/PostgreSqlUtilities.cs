using System.Text;
using System.Reflection;
using chat_systems.Message.Models;

namespace chat_systems.Shared.Utilities;

public static class PostgreSqlUtilities<T>
{
    private static List<PropertyInfo> PropertyInfos => typeof(T).GetProperties().ToList();

    public static string GenerateInsertQuery(T record, string tableName) 
    {
        var query = new StringBuilder($"INSERT INTO {tableName}");
            
        query.Append('(');

        var properties = GetPropertyNames(PropertyInfos);
        properties.ForEach(prop => { query.Append($"{prop.toSnakeCase()},"); });

        query.Remove(query.Length - 1, 1).Append(") VALUES (");

        properties.ForEach(prop => { query.Append($"@{prop},"); });

        query.Remove(query.Length - 1, 1).Append(')');

        query.Append(" RETURNING *");

        return query.ToString();
    }
    
    public static  string GenerateUpdateQuery(T record, string tableName)
    {
        var updateQuery = new StringBuilder($"UPDATE {tableName} SET ");
        var properties = GetPropertyNames(PropertyInfos);

        properties.ForEach(property =>
        {
            if (!property.Equals("Id"))
            {
                updateQuery.Append($"{property.toSnakeCase()}=@{property},");
            }
        });

        updateQuery.Remove(updateQuery.Length - 1, 1);
        updateQuery.Append(" WHERE id=@Id");

        return updateQuery.ToString();
    }

    private static List<string> GetPropertyNames(List<PropertyInfo> propertyInfos)
    {
        return propertyInfos
            .Where(propertyInfo => propertyInfo.GetCustomAttributes(typeof(IgnoreAttribute), true).Length == 0)
            .ToList()
            .ConvertAll(propertyInfo => propertyInfo.Name);
    }
}