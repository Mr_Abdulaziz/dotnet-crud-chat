using FirebaseAdmin;

namespace chat_systems.Shared.Utilities;

public static class FirebaseExceptionHandlerUtility
{
    public static T Handle<T>(Func<T> execute)
    {
        try
        {
            return execute();
        }
        catch (FirebaseException exception)
        {
            throw new Exception(exception.Message);
        }
    }
}