namespace chat_systems.Shared.Utilities;

public static class CaseConvertingUtility
{
    public static string toSnakeCase(this string str) {
        return string.Concat(str.Select((x, i) => i > 0 && char.IsUpper(x) ? "_" + x : x.ToString())).ToLower();
    }
}