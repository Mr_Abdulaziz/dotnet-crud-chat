namespace chat_systems.Shared.Constants;

public class QueryParamConstants
{
    public const string PAGE_SIZE = "page_size";

    public const string CURRENT_PAGE = "current_page";
}