namespace chat_systems.Shared.Repositories.Interfaces;

public interface IRepository<T>
{
    Task<T> SaveRecordAsync(T record, CancellationToken cancellationToken);

    Task<T> GetRecordByIdAsync(string recordId, CancellationToken cancellationToken);
    
    Task<T> UpdateRecordByRecordIdAsync(T record, string recordId, CancellationToken cancellationToken);

    Task DeleteRecordByRecordIdAsync(string recordId, CancellationToken cancellationToken);
}