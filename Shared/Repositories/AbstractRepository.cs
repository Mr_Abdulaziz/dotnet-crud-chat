using Dapper;
using Npgsql;
using chat_systems.Shared.Options;
using Microsoft.Extensions.Options;
using chat_systems.Shared.Utilities;
using chat_systems.Shared.Repositories.Interfaces;

namespace chat_systems.Shared.Repositories;

public abstract class AbstractRepository<T> : IRepository<T> where T : class
{
    private readonly PostgreSqlOptions _postgreSqlOptions;
    private readonly string tableName;
    
    protected AbstractRepository(IOptions<PostgreSqlOptions> postgreSqlOptions, string tableName)
    {
        this.tableName = tableName;
        _postgreSqlOptions = postgreSqlOptions.Value;
    }

    public async Task<T> SaveRecordAsync(T record, CancellationToken cancellationToken)
    {
        var sql = PostgreSqlUtilities<T>.GenerateInsertQuery(record, tableName);

        var commandDefinition = new CommandDefinition(sql, record, cancellationToken: cancellationToken);
        await using var connection = DBConnection();

        return await connection.QueryFirstOrDefaultAsync<T>(commandDefinition);
    }

    public async Task<T> GetRecordByIdAsync(string recordId, CancellationToken cancellationToken)
    {
        var sql = $@"SELECT * FROM {tableName} WHERE id = @Id::UUID";

        var commandDefinition = new CommandDefinition(sql, new {Id = recordId}, cancellationToken: cancellationToken);
        await using var connection = DBConnection();

        return await connection.QueryFirstOrDefaultAsync<T>(commandDefinition);
    }

    public async Task<T> UpdateRecordByRecordIdAsync(T record, string recordId, CancellationToken cancellationToken)
    {
        var sql = PostgreSqlUtilities<T>.GenerateUpdateQuery(record, tableName);
        
        var commandDefinition = new CommandDefinition(sql, record, cancellationToken: cancellationToken);
        await using var connection = DBConnection();

        return await connection.QueryFirstOrDefaultAsync<T>(commandDefinition);
    }

    public async Task DeleteRecordByRecordIdAsync(string recordId, CancellationToken cancellationToken)
    {
        var sql = $@"DELETE FROM {tableName} WHERE id = @Id::UUID";

        var commandDefinition = new CommandDefinition(sql, new {Id = recordId}, cancellationToken: cancellationToken);
        await using var connection = DBConnection();

        await connection.QueryFirstOrDefaultAsync<T>(commandDefinition);
    }
    
    protected NpgsqlConnection DBConnection() => new (_postgreSqlOptions.ConnectionString);
}
