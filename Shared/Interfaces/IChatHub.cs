namespace chat_systems.Shared.Interfaces;

public interface IChatHub
{
    Task BroadCastMessageAsync<T>(T data, CancellationToken cancellationToken);

    Task PublishToClientAsync<T>(T data, string connectionId, CancellationToken cancellationToken);

    Task PublishToCallerAsync<T>(T data, CancellationToken cancellationToken);
}