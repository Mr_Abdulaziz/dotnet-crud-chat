namespace chat_systems.Shared.Services.Interfaces;

public interface IUriService
{
    public Uri GetPaginationUri(int currentPage, int pageSize, string? route);
}