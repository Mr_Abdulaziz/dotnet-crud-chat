using chat_systems.Shared.Builders;
using chat_systems.Shared.Constants;
using chat_systems.Shared.Services.Interfaces;

namespace chat_systems.Shared.Services;

public class PaginationUriService : IUriService
{
    private readonly string _baseUrl;

    public PaginationUriService(string baseUrl)
    {
        _baseUrl = baseUrl;
    }

    public Uri GetPaginationUri(int currentPage, int pageSize, string? route)
    {
        var queryParams = new QueryStringBuilder()
            .With(QueryParamConstants.CURRENT_PAGE, currentPage.ToString())
            .With(QueryParamConstants.PAGE_SIZE, pageSize.ToString())
            .Build();
        
        return new Uri(string.Concat(_baseUrl, route, queryParams));
    }
}