using Microsoft.AspNetCore.SignalR;
using chat_systems.Shared.Interfaces;

namespace chat_systems.Shared.SignalR;

public class ChatHub : Hub, IChatHub
{
    private const string METHOD_NAME = "ReceiveMessage";
    
    public Task BroadCastMessageAsync<T>(T data, CancellationToken cancellationToken)
    {
        return Clients.All.SendAsync(METHOD_NAME, data, cancellationToken);
    }

    public Task PublishToClientAsync<T>(T data, string connectionId, CancellationToken  cancellationToken)
    {
        return Clients.Client(connectionId).SendAsync(METHOD_NAME, data, cancellationToken);
    }

    public Task PublishToCallerAsync<T>(T data, CancellationToken cancellationToken)
    {
        return Clients.Caller.SendAsync(METHOD_NAME, data, cancellationToken);
    }
}

