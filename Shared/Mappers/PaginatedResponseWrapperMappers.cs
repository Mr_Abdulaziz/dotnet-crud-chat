using chat_systems.Shared.DTOs;

namespace chat_systems.Shared.Mappers;

public static class PaginatedResponseWrapperMappers<T>
{
    public static PaginatedResponseWrapper<T> MapToPaginatedResponseWrapper(List<T> records, PaginatedResponseUri paginatedResponseUri, int currentPage, int pageSize)
    {
        return new PaginatedResponseWrapper<T>
        {
            CurrentPage = currentPage,
            PageSize = pageSize,
            PreviousPage = paginatedResponseUri.PreviousPage?.AbsoluteUri,
            NextPage = paginatedResponseUri.NextPage?.AbsoluteUri,
            Data = records
        };
    }
}