using chat_systems.Shared.Options;
using Microsoft.Extensions.Options;
using chat_systems.Conversation.Models;
using chat_systems.Shared.Repositories;
using chat_systems.Conversation.Repositories.Interfaces;

namespace chat_systems.Conversation.Repositories;

public class ConversationRepository : AbstractRepository<ConversationModel>, IConversationRepository
{
    private const string TABLE_NAME = "conversations";
    
    public ConversationRepository(IOptions<PostgreSqlOptions> postgreSqlOptions) : base(postgreSqlOptions, TABLE_NAME) { }
    
    public async Task<ConversationModel> GetConversationByConversationIdAsync(
        string conversationId, 
        CancellationToken cancellationToken)
    {
        return await GetRecordByIdAsync(conversationId, cancellationToken);
    }

    public async Task<List<ConversationModel>> GetUserRelatedConversationsByUserIdAsync(
        string userId, 
        CancellationToken cancellationToken)
    {
        return new List<ConversationModel>();
    }

    public async Task<ConversationModel> CreateConversationAsync(
        ConversationModel conversationRequest,
        CancellationToken cancellationToken)
    {
        return await SaveRecordAsync(conversationRequest, cancellationToken);
    }

    public async Task DeleteConversationByConversationIdAsync(
        string conversationId, 
        CancellationToken cancellationToken)
    {
        await DeleteRecordByRecordIdAsync(conversationId, cancellationToken);
    }
}