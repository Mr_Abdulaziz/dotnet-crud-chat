using chat_systems.Conversation.Models;

namespace chat_systems.Conversation.Repositories.Interfaces;

public interface IConversationRepository
{
    Task<ConversationModel> GetConversationByConversationIdAsync(string conversationId, CancellationToken cancellationToken);

    Task<List<ConversationModel>> GetUserRelatedConversationsByUserIdAsync(string userId, CancellationToken cancellationToken);

    Task<ConversationModel> CreateConversationAsync(ConversationModel conversationRequest, CancellationToken cancellationToken);

    Task DeleteConversationByConversationIdAsync(string conversationId, CancellationToken cancellationToken);
}