using chat_systems.Conversation.DTOs;
using chat_systems.Shared.DTOs;

namespace chat_systems.Conversation.Services.Interfaces;

public interface IConversationService
{
    Task<ConversationDTO> GetConversationByConversationIdAsync(string conversationId, CancellationToken cancellationToken);

    Task<PaginatedResponseWrapper<ConversationDTO>> GetUserRelatedConversationsByUserIdAsync(string userId, CancellationToken cancellationToken);

    Task<ConversationDTO> CreateConversationAsync(CreateConversationRequestDTO conversationRequest, CancellationToken cancellationToken);

    Task DeleteConversationByConversationIdAsync(string conversationId, CancellationToken cancellationToken);
}