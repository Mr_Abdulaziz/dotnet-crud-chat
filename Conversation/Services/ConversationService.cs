using chat_systems.Shared.DTOs;
using chat_systems.Conversation.DTOs;
using chat_systems.Conversation.Models;
using chat_systems.Conversation.Services.Interfaces;
using chat_systems.Conversation.Repositories.Interfaces;

namespace chat_systems.Conversation.Services;

public class ConversationService : IConversationService
{
    private readonly IConversationRepository _conversationRepository;

    public ConversationService(IConversationRepository conversationRepository)
    {
        _conversationRepository = conversationRepository;
    }

    public async Task<ConversationDTO> GetConversationByConversationIdAsync(
        string conversationId, 
        CancellationToken cancellationToken)
    {
        var conversationModel = await _conversationRepository.GetConversationByConversationIdAsync(conversationId, cancellationToken);
        return new ConversationDTO();
    }

    public Task<PaginatedResponseWrapper<ConversationDTO>> GetUserRelatedConversationsByUserIdAsync(
        string userId,
        CancellationToken cancellationToken)
    {
        return Task.FromResult(new PaginatedResponseWrapper<ConversationDTO>());
    }

    public async Task<ConversationDTO> CreateConversationAsync(
        CreateConversationRequestDTO conversationRequest,
        CancellationToken cancellationToken)
    {
        var conversationModel = await _conversationRepository.CreateConversationAsync(new ConversationModel(), cancellationToken);
        return new ConversationDTO();
    }

    public async Task DeleteConversationByConversationIdAsync(
        string conversationId,
        CancellationToken cancellationToken)
    {
        await _conversationRepository.DeleteConversationByConversationIdAsync(conversationId, cancellationToken);
    }
}