using chat_systems.Shared.DTOs;
using Microsoft.AspNetCore.Mvc;
using chat_systems.Conversation.DTOs;

namespace chat_systems.Controllers;

[ApiController]
[Route("api/v1/conversations")]
[Produces("application/json")]
public class ConversationController : ControllerBase
{
    [HttpGet("{conversationId}")]
    public async Task<ConversationDTO> GetConversationByConversationId(string conversationId)
    {
        return new ConversationDTO();
    }

    [HttpGet]
    public async Task<PaginatedResponseWrapper<ConversationDTO>> GetUserRelatedConversationsByUserId(string userId)
    {
        return new PaginatedResponseWrapper<ConversationDTO>();
    }

    [HttpPost]
    public async Task<ConversationDTO> CreateConversation(
        [FromBody] 
        CreateConversationRequestDTO createConversationRequest)
    {
        return new ConversationDTO();
    }

    [HttpDelete("{conversationId}")]
    public async Task DeleteConversationByConversationId(string conversationId)
    {
        
    }
}