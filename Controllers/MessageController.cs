using MediatR;
using Microsoft.AspNetCore.Mvc;
using chat_systems.Message.DTOs;
using chat_systems.Message.Mediator.Queries;
using chat_systems.Message.Mediator.Commands;

namespace chat_systems.Controllers;

[ApiController]
[Route("api/v1/messages")]
[Produces("application/json")]
public class MessageController : ControllerBase
{
    private readonly IMediator _mediator;

    public MessageController(IMediator mediator)
    {
        _mediator = mediator;
    }

    [HttpGet("{messageId}")]
    public async Task<MessageDTO> GetMessageByMessageIdAsync(
        string messageId, 
        CancellationToken cancellationToken)
    {
        var getMessageQuery = new GetMessageByMessageIdQuery.Request(messageId);
        return await _mediator.Send(getMessageQuery, cancellationToken);
    }
    
    [HttpPost]
    public async Task<MessageDTO> CreateMessageAsync(
        [FromBody] CreateMessageRequestDTO createMessageRequest, 
        CancellationToken cancellationToken)
    {
        var createMessageCommand = new CreateMessageCommand.Request(createMessageRequest);
        return await _mediator.Send(createMessageCommand, cancellationToken);
    }
    
    [HttpPut("{messageId}")]
    public async Task<MessageDTO> UpdateMessageByMessageIdAsync(
        string messageId, 
        [FromBody] UpdateMessageRequestDTO updateMessageRequest,
        CancellationToken cancellationToken)
    {
        var updateMessageCommand = new UpdateMessageByMessageIdCommand.Request(messageId, updateMessageRequest);
        return await _mediator.Send(updateMessageCommand, cancellationToken);
    }

    [HttpDelete("{messageId}")]
    public async Task DeleteMessageByMessageIdAsync(
        string messageId, 
        CancellationToken cancellationToken)
    {
        var deleteMessageCommand = new DeleteMessageByMessageIdCommand.Request(messageId);
        await _mediator.Send(deleteMessageCommand, cancellationToken);
    }
}