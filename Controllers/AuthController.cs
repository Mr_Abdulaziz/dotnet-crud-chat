using MediatR;
using Microsoft.AspNetCore.Mvc;
using chat_systems.Authorization.DTOs;
using Microsoft.AspNetCore.Authorization;
using chat_systems.Authorization.Mediator.Commands;

namespace chat_systems.Controllers;

[ApiController]
[Route("api/v1/auth")]
[Produces("application/json")]
public class AuthController : ControllerBase
{
    private readonly IMediator _mediator;

    public AuthController(IMediator mediator)
    {
        _mediator = mediator;
    }

    [AllowAnonymous]
    [HttpPost("/sign-in")]
    public async Task<string> SignInAsync([FromBody] AuthSignInRequestDTO authSignInRequest)
    {
        var signInCommand = new SignInCommand.Request(authSignInRequest);
        return await _mediator.Send(signInCommand);
    }

    [AllowAnonymous]
    [HttpPost("/custom-token")]
    public async Task<string> CreateCustomTokenAsync([FromBody] CustomTokenRequestDTO customTokenRequest)
    {
        var createCustomTokenCommand = new CreateCustomTokenCommand.Request(customTokenRequest);
        return await _mediator.Send(createCustomTokenCommand);
    }
}