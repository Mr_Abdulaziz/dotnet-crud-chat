using chat_systems.Authorization.DTOs;

namespace chat_systems.Authorization.Interfaces;

public interface IAuthService
{
    Task<AuthServiceResponseDTO?> GetUserByPhoneNumberAsync(string phoneNumber);

    Task<AuthServiceResponseDTO?> CreateUserAsync();

    Task DeleteUserAsync(string uuid);
}