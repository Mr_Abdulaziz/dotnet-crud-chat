using FirebaseAdmin.Auth;
using chat_systems.Authorization.Interfaces;
using chat_systems.Shared.Options;
using FirebaseAdmin;
using Google.Apis.Auth.OAuth2;

namespace chat_systems.Authorization.Extensions;

public static class AuthServiceExtensions
{
    public static IServiceCollection AddAuthServices(this IServiceCollection serviceCollection, IConfiguration configuration)
    {
        configuration.ConfigureFirebaseApp();
        
        serviceCollection.AddSingleton(typeof(IAuthService), typeof(FirebaseAuth));
        
        return serviceCollection;
    }
    
    private static void ConfigureFirebaseApp(this IConfiguration configuration)
    {
        var googleOptions = configuration.GetSection(GoogleClientOptions.SECTION_NAME).Get<GoogleClientOptions>();

        foreach (var dict in googleOptions.FirebaseApp.Credentials)
        {
            var credential = GoogleCredential.FromJson(dict.Value);
            var options = new AppOptions { Credential = credential };

            FirebaseApp.Create(options, dict.Key);
        }
    }
}