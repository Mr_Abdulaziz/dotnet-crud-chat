using FirebaseAdmin;
using FirebaseAdmin.Auth;
using chat_systems.Shared.Utilities;
using chat_systems.Authorization.DTOs;
using chat_systems.Authorization.Interfaces;

namespace chat_systems.Authorization.Services;

public class FirebaseAuthService : IAuthService
{
    private readonly FirebaseAuth _auth = GetFirebaseAuth();
    private const string PROJECT_ID = "chat_sytems_dev";

    public async Task<AuthServiceResponseDTO?> GetUserByPhoneNumberAsync(string phoneNumber)
    {
        await FirebaseExceptionHandlerUtility.Handle(
            async () => await _auth.GetUserByPhoneNumberAsync(phoneNumber));

        return new AuthServiceResponseDTO();
    }

    public async Task<UserRecord> UpdateAuthUserAsync(UserRecordArgs args)
    {
        return await FirebaseExceptionHandlerUtility.Handle(
            async () => await _auth.UpdateUserAsync(args));
    }
    
    public async Task<AuthServiceResponseDTO?> CreateUserAsync()
    {
        await FirebaseExceptionHandlerUtility.Handle(
            async () => await _auth.CreateUserAsync(new UserRecordArgs()));

        return new AuthServiceResponseDTO();
    }

    public async Task DeleteUserAsync(string uuid)
    {
        await FirebaseExceptionHandlerUtility.Handle(async () => await _auth.DeleteUserAsync(uuid));
    }

    private static FirebaseAuth GetFirebaseAuth()
    {
        var app = FirebaseApp.GetInstance(PROJECT_ID);
        
        return FirebaseAuth.GetAuth(app);
    }
}