namespace chat_systems.Authorization.DTOs;

public class CustomTokenRequestDTO
{
    public string FirstName { get; set; }
    
    public string LastName { get; set; }
    
    public string? PhoneNumber { get; set; }
    
    public string AuthCode { get; set; }
    
    public string Otp { get; set; }
}