namespace chat_systems.Authorization.DTOs;

public class AuthSignInRequestDTO
{
    public string PhoneNumber { get; set; }
}