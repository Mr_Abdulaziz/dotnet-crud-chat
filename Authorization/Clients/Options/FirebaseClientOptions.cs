namespace chat_systems.Authorization.Clients.Options;

public class FirebaseClientOptions
{
    public Dictionary<string, string> Credentials { get; set; }
    
    public string ProjectId { get; set; }
}