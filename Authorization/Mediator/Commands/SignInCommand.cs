using MediatR;
using chat_systems.Authorization.DTOs;

namespace chat_systems.Authorization.Mediator.Commands;

public class SignInCommand
{
    public record Request(AuthSignInRequestDTO CustomTokenRequest) : IRequest<string>;
    
    public class Handler : IRequestHandler<Request, string>
    {
        public Task<string> Handle(Request request, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}