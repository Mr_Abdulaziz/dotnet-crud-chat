using MediatR;
using chat_systems.Message.Services.Interfaces;

namespace chat_systems.Message.Mediator.Commands;

public class DeleteMessageByMessageIdCommand
{
    public record Request(string MessageId) : IRequest;
    
    public class Handler : IRequestHandler<Request>
    {
        private readonly IMessageService _messageService;
        
        public Handler(IMessageService messageService)
        {
            _messageService = messageService;
        }
         
        public async Task Handle(Request request, CancellationToken cancellationToken)
        {
            await _messageService.DeleteMessageByMessageIdAsync(request.MessageId, cancellationToken);
        }
    }
}