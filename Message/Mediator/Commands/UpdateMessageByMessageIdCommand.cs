using MediatR;
using chat_systems.Message.DTOs;
using chat_systems.Message.Services.Interfaces;

namespace chat_systems.Message.Mediator.Commands;

public class UpdateMessageByMessageIdCommand
{
    public record Request(string MessageId, UpdateMessageRequestDTO UpdateMessageRequest) : IRequest<MessageDTO>;
    
    public class Handler : IRequestHandler<Request, MessageDTO>
    {
        private readonly IMessageService _messageService;
        
        public Handler(IMessageService messageService)
        {
            _messageService = messageService;
        }
         
        public async Task<MessageDTO> Handle(Request request, CancellationToken cancellationToken)
        {
            return await _messageService.UpdateMessageByMessageIdAsync(request.MessageId, request.UpdateMessageRequest, cancellationToken);
        }
    }
}