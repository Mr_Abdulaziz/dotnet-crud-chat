using MediatR;
using chat_systems.Message.DTOs;
using chat_systems.Message.Services.Interfaces;

namespace chat_systems.Message.Mediator.Queries;

public class GetMessageByMessageIdQuery
{
    public record Request(string MessageId) : IRequest<MessageDTO>;
    
    public class Handler : IRequestHandler<Request, MessageDTO>
    {
        private readonly IMessageService _messageService;

        public Handler(IMessageService messageService)
        {
            _messageService = messageService;
        }

        public Task<MessageDTO> Handle(Request request, CancellationToken cancellationToken)
        {
            return _messageService.GetMessageByMessageIdAsync(request.MessageId, cancellationToken);
        }
    }
}