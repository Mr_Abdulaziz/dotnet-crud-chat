using MediatR;
using chat_systems.Message.DTOs;
using chat_systems.Message.Services.Interfaces;
using chat_systems.Shared.DTOs;

namespace chat_systems.Message.Mediator.Queries;

public class GetChatRelatedMessagesByChatIdQuery
{
    public record Request(string ChatId, int CurrentPage, int PageSize, string? Route) : IRequest<PaginatedResponseWrapper<MessageDTO>>;
    
    public class Handler : IRequestHandler<Request, PaginatedResponseWrapper<MessageDTO>>
    {
        private readonly IMessageService _messageService;

        public Handler(IMessageService messageService)
        {
            _messageService = messageService;
        }

        public Task<PaginatedResponseWrapper<MessageDTO>> Handle(Request request, CancellationToken cancellationToken)
        {
            return _messageService.GetChatRelatedMessagesByChatIdAsync(request.ChatId, request.CurrentPage, request.PageSize, request.Route, cancellationToken);
        }
    }
}