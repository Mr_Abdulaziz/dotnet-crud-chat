using chat_systems.Shared.Enums;

namespace chat_systems.Message.DTOs;

public record UpdateMessageRequestDTO
{
    public MessageTypeEnum MessageType { get; set; }
    
    public string Captions { get; set; }
}