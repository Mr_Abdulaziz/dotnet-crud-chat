using chat_systems.Shared.Enums;

namespace chat_systems.Message.DTOs;

public record MessageDTO
{
    public string Id { get; set; }
    
    public MessageTypeEnum MessageType { get; set; }
    
    public string Captions { get; set; }
    
    public DateTimeOffset CreatedAt { get; set; }
}