using Dapper;
using chat_systems.Message.Models;
using chat_systems.Shared.Options;
using Microsoft.Extensions.Options;
using chat_systems.Shared.Repositories;
using chat_systems.Message.Repositories.Interfaces;

namespace chat_systems.Message.Repositories;

public class MessageRepository : AbstractRepository<MessageModel>, IMessageRepository
{
    private const string TABLE_NAME = "messages";

    public MessageRepository(IOptions<PostgreSqlOptions> postgreSqlOptions) : base(postgreSqlOptions, TABLE_NAME) { }

    // TODO: compare with cursor-based pagination 
    public async Task<List<MessageModel>> GetChatRelatedMessagesByChatIdAsync(
        string chatId, 
        int currentPage, 
        int pageSize, 
        CancellationToken cancellationToken)
    {
        var sql = $@"
            SELECT * FROM {TABLE_NAME}
            WHERE chat_id = @chatID::UUID
            ORDER BY id
            OFFSET {(currentPage - 1) * pageSize}
            FETCH NEXT {pageSize} ROWS ONLY
        ";

        var commandDefinition = new CommandDefinition(sql, new {chatId = chatId});
        await using var connection = DBConnection();
        
        var messageModels = await connection.QueryAsync<MessageModel>(commandDefinition);

        return messageModels.ToList();
    }
    
    public async Task<MessageModel> SaveMessageAsync(MessageModel messageModel, CancellationToken cancellationToken)
    {
        return await SaveRecordAsync(messageModel, cancellationToken);
    }

    public async Task<MessageModel> GetMessageByMessageIdAsync(string messageId, CancellationToken cancellationToken)
    {
        var sql = $@"
            SELECT id, chat_id, captions, message_type,
            TO_CHAR(created_at, 'YYYY-MM-DD""T""HH24:MI:SSOF') as created_at
            FROM messages WHERE id = @Id::UUID
        ";
        
        var commandDefinition = new CommandDefinition(sql, new {Id = messageId});
        await using var connection = DBConnection();
        
        var messageModel = await connection.QueryFirstOrDefaultAsync<MessageModel>(commandDefinition);
        return messageModel;
    }

    public async Task<MessageModel> UpdateMessageByMessageIdAsync(MessageModel messageModel, string messageId, CancellationToken cancellationToken)
    {
        return await UpdateRecordByRecordIdAsync(messageModel, messageId, cancellationToken);
    }

    public async Task DeleteMessageByMessageIdAsync(string messageId, CancellationToken cancellationToken)
    {
        await DeleteRecordByRecordIdAsync(messageId, cancellationToken);
    }
}