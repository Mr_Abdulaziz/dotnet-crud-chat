using chat_systems.Message.Models;

namespace chat_systems.Message.Repositories.Interfaces;

public interface IMessageRepository
{
    Task<MessageModel> SaveMessageAsync(MessageModel messageModel, CancellationToken cancellationToken);

    Task<MessageModel> GetMessageByMessageIdAsync(string messageId, CancellationToken cancellationToken);

    Task<List<MessageModel>> GetChatRelatedMessagesByChatIdAsync(
        string chatId,
        int currentPage,
        int pageSize,
        CancellationToken cancellationToken);

    Task<MessageModel> UpdateMessageByMessageIdAsync(
        MessageModel messageModel, string messageId,
        CancellationToken cancellationToken);

    Task DeleteMessageByMessageIdAsync(string messageId, CancellationToken cancellationToken);
}