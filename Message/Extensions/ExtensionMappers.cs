using chat_systems.Message.DTOs;
using chat_systems.Message.Models;

namespace chat_systems.Message.Extensions;

public static class ExtensionMappers
{
    public static MessageModel ToMessageModel(this CreateMessageRequestDTO createMessageRequestDto) => new()
    {
        Captions = createMessageRequestDto.Captions,
        CreatedAt = DateTime.UtcNow
    };

    public static MessageModel ToMessageModel(this UpdateMessageRequestDTO updateMessageRequestDto, string messageId) => new()
    {
        Captions = updateMessageRequestDto.Captions,
        MessageType = updateMessageRequestDto.MessageType
    };

    public static MessageModel ToMessageModel(this MessageDTO messageDto) => new()
    {
        Captions = messageDto.Captions,
        MessageType = messageDto.MessageType
    };

    public static MessageDTO ToMessageDTO(this MessageModel messageModel) => new()
    {
        Id = messageModel.Id.ToString(),
        Captions = messageModel.Captions,
        CreatedAt = messageModel.CreatedAt,
        MessageType = messageModel.MessageType
    };

    public static List<MessageDTO> ToMessageDTOs(this List<MessageModel> messageModels) => messageModels.ConvertAll(
        model => new MessageDTO()
        {
            Id = model.Id.ToString(),
            Captions = model.Captions,
            CreatedAt = model.CreatedAt,
            MessageType = model.MessageType
        });
}