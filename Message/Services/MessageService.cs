using chat_systems.Shared.DTOs;
using chat_systems.Message.DTOs;
using chat_systems.Shared.Mappers;
using chat_systems.Message.Extensions;
using chat_systems.Shared.Services.Interfaces;
using chat_systems.Message.Services.Interfaces;
using chat_systems.Message.Repositories.Interfaces;

namespace chat_systems.Message.Services;

public class MessageService : IMessageService
{
    private readonly IMessageRepository _messageRepository;

    private readonly IUriService _uriService;

    public MessageService(IMessageRepository messageRepository, IUriService uriService)
    {
        _messageRepository = messageRepository;
        _uriService = uriService;
    }

    public async Task<MessageDTO> GetMessageByMessageIdAsync(
        string messageId, 
        CancellationToken cancellationToken)
    {
        var messageModel = await _messageRepository.GetMessageByMessageIdAsync(messageId, cancellationToken);
        return messageModel.ToMessageDTO();
    }

    public async Task<MessageDTO> CreateMessageAsync(
        CreateMessageRequestDTO createMessageRequest, 
        CancellationToken cancellationToken)
    {
        var messageModel = await _messageRepository.SaveMessageAsync(createMessageRequest.ToMessageModel(), cancellationToken);
        return messageModel.ToMessageDTO();
    }

    public async Task<MessageDTO> UpdateMessageByMessageIdAsync(
        string messageId, 
        UpdateMessageRequestDTO updateMessageRequest,
        CancellationToken cancellationToken)
    {
        var messageModel = await _messageRepository.UpdateMessageByMessageIdAsync(updateMessageRequest.ToMessageModel(messageId), messageId, cancellationToken);
        return messageModel.ToMessageDTO();
    }

    public async Task<PaginatedResponseWrapper<MessageDTO>> GetChatRelatedMessagesByChatIdAsync(
        string chatId, 
        int currentPage,
        int pageSize,
        string? route,
        CancellationToken cancellationToken)
    {
        var messageModels = await _messageRepository.GetChatRelatedMessagesByChatIdAsync(chatId, currentPage, pageSize, cancellationToken);
        var messageDTOs = messageModels.ToMessageDTOs();
        var paginatedResponseUri = new PaginatedResponseUri
        {
            PreviousPage = currentPage > 1 && messageDTOs.Count is not 0 ? _uriService.GetPaginationUri(currentPage - 1, pageSize, route) : null,
            NextPage = _uriService.GetPaginationUri(currentPage + 1, pageSize, route)
        };
        return PaginatedResponseWrapperMappers<MessageDTO>.MapToPaginatedResponseWrapper(messageDTOs, paginatedResponseUri, currentPage, pageSize);
    }

    public async Task DeleteMessageByMessageIdAsync(
        string messageId, 
        CancellationToken cancellationToken)
    {
        await _messageRepository.DeleteMessageByMessageIdAsync(messageId, cancellationToken);
    }
}