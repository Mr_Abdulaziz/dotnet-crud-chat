using chat_systems.Message.DTOs;
using chat_systems.Shared.DTOs;

namespace chat_systems.Message.Services.Interfaces;

public interface IMessageService
{
    Task<MessageDTO> GetMessageByMessageIdAsync(string messageId, CancellationToken cancellationToken);

    Task<MessageDTO> CreateMessageAsync(CreateMessageRequestDTO createMessageRequest, CancellationToken cancellationToken);

    Task<MessageDTO> UpdateMessageByMessageIdAsync(
        string messageId,
        UpdateMessageRequestDTO updateMessageRequest,
        CancellationToken cancellationToken);

    Task<PaginatedResponseWrapper<MessageDTO>> GetChatRelatedMessagesByChatIdAsync(string chatId, int currentPage, int pageSize, string? Route, CancellationToken cancellationToken);

    Task DeleteMessageByMessageIdAsync(string messageId, CancellationToken cancellationToken);
}