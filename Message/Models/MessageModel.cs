using chat_systems.Shared.Enums;

namespace chat_systems.Message.Models;

public class MessageModel
{
    [Ignore]
    public Guid Id { get; set; }
    
    public MessageTypeEnum MessageType { get; set; }
    
    public string Captions { get; set; }

    [Ignore]
    public DateTimeOffset CreatedAt { get; set; }
}

public class IgnoreAttribute : Attribute { }

